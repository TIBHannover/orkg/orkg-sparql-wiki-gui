FROM node:lts-buster as build

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json package-lock.json ./

RUN npm clean-install
RUN npm install grunt-cli

COPY . ./

RUN npm run build




FROM nginx:stable-alpine

COPY --from=build /app/build /usr/share/nginx/html/
