# ORKG SPARQL GUI

![Example query with timeline](/images/timeline-orkg.gif)

An altered version of wikidata query gui to work on the ORKG data model.

Based on https://github.com/wikimedia/wikidata-query-gui

## Configuration
Per default the Wikibase Query Service GUI is configured to be used as a local development test instance. It can be customized by creating a `custom-config.json` in the repository's root dir. This file can be used to override any of the default settings obtained from `default-config.json`.

## Run tests

Run JSHint, JSCS and QUnit tests.

```bash
$ npm test
```

## Debug
Start a test server for local debugging. Do not use it in production.

```bash
$ npm start
```

## Build
Create a build with bundled and minified files.

```bash
$ npm run build
```


## Deploy
Create a build and push it to the deployment branch via git review.

```bash
$ npm run deploy
```
